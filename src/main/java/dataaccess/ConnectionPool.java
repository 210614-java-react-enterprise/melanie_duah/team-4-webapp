package dataaccess;

import java.sql.Connection;

/**
 * An object that creates and maintains a pool of JDBC connections
 */
public interface ConnectionPool {

     /**
      * Initializes this connection pool object with database connection credentials. It may also create a single initial
      * connection
      * @param databaseUrl the connection url of the database server
      * @param schema the schema to use for all connections
      * @param user a user name of the server to use for connection
      * @param password  a password to the server to use for connection
      * @param poolSize the maximum number of connections in the pool
      * @throws DataSourceException
      */
     void initialize(String databaseUrl, String schema, String user, String password, int poolSize)
             throws DataSourceException;


     /**
      * Gets a Connection object from the pool. May create the connection object of all existing ones are used up and
      * there is still enough room to create one based on the given pool size
      * @return @see java.sql.Connection
      * @throws DataSourceException
      */
     Connection getConnection() throws DataSourceException;

     /**
      * Releases a Connection object and returns it to the pool
      *
      * @param connection
      */
     void releaseConnection(Connection connection);
}
