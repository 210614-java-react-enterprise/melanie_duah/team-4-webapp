package dataaccess;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DAOImpl<T> implements DAO<T> {

    private ConnectionPool connectionPool;
    private Class<T> modelClass;

    public DAOImpl(ConnectionPool connectionPool, Class<T> modelClass) {
        this.connectionPool = connectionPool;
        this.modelClass = modelClass;
    }

    @Override
    public void create(T model) throws DAOException {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String tableName = ReflectionHelper.getTableName(modelClass);
            ColumnSql columnSql = ReflectionHelper.getInsertColumnsSql(modelClass);
            String sql = QueryBuilder.getInsert(tableName, columnSql.getColumnNames(), columnSql.getValuePlaceHolders());
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ReflectionHelper.setPreparedStatementParameters(model, preparedStatement);
            preparedStatement.executeUpdate();

        } catch (DataSourceException | SQLException | IllegalAccessException e) {
            throw new DAOException("Error persisting model", e);
        } finally {
            if (connection != null)
                connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public void update(T model) throws DAOException {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String tableName = ReflectionHelper.getTableName(modelClass);
            ColumnSql columnSql = ReflectionHelper.getUpdateColumnSql(modelClass);
            String sql = QueryBuilder.getUpdate(tableName, columnSql.getColumnNames(), columnSql.getWhereConditions());
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ReflectionHelper.setUpdatePreparedStatement(model, preparedStatement);
            preparedStatement.executeUpdate();

        } catch (DataSourceException | SQLException | IllegalAccessException e) {
            throw new DAOException("Error updating model", e);
        } finally {
            if (connection != null)
                connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public void delete(T model) throws DAOException {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String tableName = ReflectionHelper.getTableName(modelClass);
            ColumnSql columnsSql = ReflectionHelper.getWhereIdCondition(modelClass);
            String sql = QueryBuilder.getDelete(tableName, columnsSql.getWhereConditions());
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ReflectionHelper.setIDPreparedStatement(model, preparedStatement);
            preparedStatement.executeUpdate();

        } catch (DataSourceException | SQLException | IllegalAccessException e) {
            throw new DAOException("Error in deleting table", e);
        } finally {
            if (connection != null)
                connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public T find(Object primaryKey) throws DAOException {
        Connection connection = null;
        T model = null;
        try {
            connection = connectionPool.getConnection();
            String tableName = ReflectionHelper.getTableName(modelClass);
            ColumnSql columnSql = ReflectionHelper.getSelectColumns(modelClass);
            String sql = QueryBuilder.getSelect(tableName, columnSql.getColumnNames(), columnSql.getWhereConditions());
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setObject(1, primaryKey);
            ResultSet results = preparedStatement.executeQuery();

            if (results.next()) {
                model = ReflectionHelper.createModelFromResult(results, modelClass);
            }

        } catch (DataSourceException | SQLException | NoSuchMethodException |
                InvocationTargetException | IllegalAccessException | InstantiationException e) {
            throw new DAOException("Error finding model", e);
        } finally {
            if (connection != null)
                connectionPool.releaseConnection(connection);
        }
        return model;
    }

    @Override
    public List<T> findAll() throws DAOException {
        Connection connection = null;
        List<T> models = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            String tableName = ReflectionHelper.getTableName(modelClass);
            ColumnSql columnSql = ReflectionHelper.getAllSelectColumns(modelClass);
            String sql = QueryBuilder.selectAll(tableName, columnSql.getColumnNames());
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet results = preparedStatement.executeQuery();

            while (results.next()) {
                T model = ReflectionHelper.createModelFromResult(results, modelClass);
                models.add(model);
            }
        } catch (DataSourceException | SQLException |
                NoSuchMethodException | InvocationTargetException |
                InstantiationException | IllegalAccessException e) {
            throw new DAOException("Error finding all model", e);
        } finally {
            if (connection != null)
                connectionPool.releaseConnection(connection);
        }
        return models;
    }

    @Override
    public List<T> findWhere(Map<String, Object> properties) throws DAOException {
        Connection connection = null;
        List<T> models = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            String tableName = ReflectionHelper.getTableName(modelClass);
            StringBuilder whereBuilder = new StringBuilder();
            for (String column : properties.keySet()) {
                whereBuilder.append(column + " = ?");
            }
            ColumnSql columnSql = ReflectionHelper.getAllSelectColumns(modelClass);
            String sql = QueryBuilder.getSelect(tableName, columnSql.getColumnNames(), whereBuilder.toString());
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            Object[] values = properties.values().toArray();
            for (int i = 0; i < values.length; i++) {
                Object value = values[i];
                preparedStatement.setObject(i + 1, value);
            }
            ResultSet results = preparedStatement.executeQuery();

            while (results.next()) {
                T model = ReflectionHelper.createModelFromResult(results, modelClass);
                models.add(model);
            }
        } catch (DataSourceException | SQLException |
                NoSuchMethodException | InvocationTargetException |
                InstantiationException | IllegalAccessException e) {
            throw new DAOException("Error finding all model", e);
        } finally {
            if (connection != null)
                connectionPool.releaseConnection(connection);
        }
        return models;
    }
}
