package dataaccess;

import dataaccess.annotations.Entity;

import java.lang.annotation.Annotation;
import java.util.*;

public class DAOCreatorImpl implements DAOCreator {

    private static DAOCreatorImpl instance = null;
    private ConnectionPool connectionPool;
    private Map<String, DAO<?>> daoStore;

    public synchronized static DAOCreatorImpl getInstance() {
        if (instance == null) {
            instance = new DAOCreatorImpl();
        }
        return instance;
    }

    private DAOCreatorImpl() {
        this.daoStore = new Hashtable<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConnectionPool(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> DAO<T> getDAO(Class<T> modelClass) throws DataSourceException {
        if (connectionPool == null)
            throw new DataSourceException("No connection source");

        if (!modelClass.isAnnotationPresent(Entity.class))
            throw new IllegalStateException("Given model class is not an Entity");

        DAO<T> dao;
        String daoKey = modelClass.getSimpleName();

        if (daoStore.containsKey(daoKey)) {
            dao = (DAO<T>) daoStore.get(daoKey);
        } else {
            dao = new DAOImpl<T>(connectionPool, modelClass);
            daoStore.put(daoKey, dao);
        }

        return dao;
    }
}
