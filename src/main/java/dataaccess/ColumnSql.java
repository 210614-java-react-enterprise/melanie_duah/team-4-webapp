package dataaccess;

public class ColumnSql {
    private String columnNames;
    private String valuePlaceHolders;
    private String whereConditions;

    public String getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(String columnNames) {
        this.columnNames = columnNames;
    }

    public String getValuePlaceHolders() {
        return valuePlaceHolders;
    }

    public void setValuePlaceHolders(String valuePlaceHolders) {
        this.valuePlaceHolders = valuePlaceHolders;
    }

    public String getWhereConditions() {
        return whereConditions;
    }

    public void setWhereConditions(String whereConditions) {
        this.whereConditions = whereConditions;
    }
}
