package util;

public class HtmlHelper {

	public static final String lineBreak = "<br/>";

	public static final String tableHeader =
			"< tr>" +
				"<th> Name </th>"	+
				"<th> Systems </th>"	+
				"<th> Description </th>" +
				"<th> US Release </th>" +
				"<th> JP Release </th>" +
				"<th> EU Release </th>" +
				"<th> Developer </th>"	+
				"<th> Publisher </th>"	+
				"<th> Publisher notes </th>"	+
				"<th> Release date notes </th>"	+
			"</tr>";

	public static String wrapCell(String cell) {
		StringBuilder builder = new StringBuilder();

		builder.append("<td>");
		builder.append(cell);
		builder.append("</td>");

		return builder.toString();
	}

	public static String wrapHtml(String html) {
		StringBuilder builder = new StringBuilder();

		builder.append("<html><body>");
		builder.append(html);
		builder.append("</body></html>");

		return builder.toString();
	}

	public static String wrapRow(String record) {
		StringBuilder builder = new StringBuilder();

		builder.append("<tr>");
		builder.append(record);
		builder.append("</tr>");

		return builder.toString();
	}

	public static String wrapTable(String table) {
		StringBuilder builder = new StringBuilder();

		builder.append("<table>");
		builder.append(table);
		builder.append("</table>");

		return builder.toString();
	}
}
