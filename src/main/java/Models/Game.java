package Models;

import dataaccess.annotations.*;

import java.util.Objects;

@Entity
@Table(name = "game")
public class Game {
    @Id
    @GeneratedValue
    @Column(name = "game_id")
    private int id;

    @Column(name = "title")
    private String name;

    @Column(name = "description")
    private String description;

    @JoinColumn(name = "genre_id", referenceColumnName = "genre_id")
    private Genre genre;

    @JoinColumn(name = "rel_date_id",referenceColumnName = "rel_date_id")
    private ReleaseDate releaseDate;

    public ReleaseDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(ReleaseDate releaseDate) {
        this.releaseDate = releaseDate;
    }


    public Game() {
        super();
    }


    public Game(String name, String description,ReleaseDate releaseDate,Genre genre) {

    public Game(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Game(String name, String description) {
        this.name = name;
        this.description = description;
        this.releaseDate=releaseDate;
        this.genre=genre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Games{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return id == game.id && Objects.equals(name, game.name) && Objects.equals(description, game.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
