package Models;

import dataaccess.annotations.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "rel_date")
public class ReleaseDate {
    @Id
    @GeneratedValue
    @Column(name = "rel_date_id")
    private int id;

    @Column(name = "us")
    private LocalDate us;

    @Column(name = "eu")
    private LocalDate eu;

    @Column(name = "jp")
    private LocalDate jp;

    @Column(name = "notes")
    private String notes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public ReleaseDate() {
        super();
    }

    public ReleaseDate(LocalDate us, LocalDate eu, LocalDate jp, String notes) {
        this.us = us;
        this.eu = eu;
        this.jp = jp;
        this.notes = notes;
    }

    public LocalDate getUs() {
    public ReleaseDate(Game game) {
        this.game = game;
    }

    public LocalDateTime getUs() {
        return us;
    }

    public void setUs(LocalDate us) {
        this.us = us;
    }

    public LocalDate getEu() {
        return eu;
    }

    public void setEu(LocalDate eu) {
        this.eu = eu;
    }

    public LocalDate getJp() {
        return jp;
    }

    public void setJp(LocalDate jp) {
        this.jp = jp;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "ReleaseDate{" +
                "game=" +
                ", us=" + us +
                ", eu=" + eu +
                ", jp=" + jp +
                ", notes='" + notes + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReleaseDate that = (ReleaseDate) o;
        return Objects.equals(us, that.us) && Objects.equals(eu, that.eu) && Objects.equals(jp, that.jp) && Objects.equals(notes, that.notes);
    }

    @Override
    public int hashCode() {
        return Objects.hash( us, eu, jp, notes);
    }
}
