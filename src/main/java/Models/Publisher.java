package Models;

import dataaccess.annotations.*;

import java.util.Objects;

@Entity
@Table
public class Publisher {

    @Id
    @GeneratedValue
    @Column
    private int id;

  @JoinColumn(name = "game",referenceColumnName = "game_id")
    private Game game;

    @Column(name = "publisher")
    private String name;

   @JoinColumn(name = "notes",referenceColumnName = "id")
    private PublisherNote publisherNote;

    public PublisherNote getPublisherNote() {
        return publisherNote;
    }

    public void setPublisherNote(PublisherNote publisherNote) {
        this.publisherNote = publisherNote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Publisher(){
        super();
    }

    public Publisher( String name,PublisherNote publisherNote,Game game) {
=======
    public Publisher(String name, Game game) {
        this.game=game;
        this.name = name;
       this.publisherNote=publisherNote;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Publishers{" +
                "game=" + game +
                ", studio_name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publisher publisher = (Publisher) o;
        return game.equals(publisher.game) && name.equals(publisher.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(game, name);
    }
}
