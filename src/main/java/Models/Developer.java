package Models;

import dataaccess.annotations.*;

import java.util.Objects;

@Entity
@Table
public class Developer {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @JoinColumn(name = "game",referenceColumnName = "game_id")
    private Game game;

    @Column(name = "studio")
    private String studio_name;

    @JoinColumn(name = "system",referenceColumnName = "id")
    private SystemImplementation systemImplementation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Developer(){
        super();
    }

    public Developer(Game game, String studio_name,SystemImplementation systemImplementation) {

    public Developer(int id, Game game, String studio_name){
        this.id = id;
        this.game = game;
        this.studio_name = studio_name;
    }

    public Developer(Game game, String studio_name) {

        this.game = game;
        this.studio_name = studio_name;
        this.systemImplementation = systemImplementation;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public String getStudio_name() {
        return studio_name;
    }

    public void setStudio_name(String studio_name) {
        this.studio_name = studio_name;
    }

    @Override
    public String toString() {
        return "Devs{" +
                "game=" + game +
                ", studio_name='" + studio_name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Developer developer = (Developer) o;
        return game.equals(developer.game) && studio_name.equals(developer.studio_name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(game, studio_name);
    }
}
