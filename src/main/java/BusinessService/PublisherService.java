package BusinessService;

import Models.*;
import java.util.*;

public interface PublisherService {

    void addPublisher(String name, PublisherNote publisherNote,Game game) throws BusinessException;

    void addPublisherNote(String name) throws BusinessException;

    List<Publisher> getAllPublishers() throws BusinessException;

    Publisher findPublisher(int id) throws BusinessException;

    void removePublisher(int id) throws BusinessException;

    Publisher updatePublisher(int id) throws BusinessException;

}
