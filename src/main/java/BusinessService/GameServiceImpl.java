package BusinessService;

import Models.*;
import dataaccess.DAO;
import dataaccess.DAOCreator;
import dataaccess.DAOException;
import dataaccess.DataSourceException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * {@inheritDoc}
 */
public class GameServiceImpl implements GameService {

    private final DAOCreator daoCreator;

    public GameServiceImpl(DAOCreator daoCreator) {
        this.daoCreator = daoCreator;
    }

    @Override
    public void addGenre(String name) throws BusinessException {
        try {
            DAO<Genre> genreDAO = daoCreator.getDAO(Genre.class);
            Genre genre = new Genre(name);
            genreDAO.create(genre);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public Genre findGenreByName(String name) throws BusinessException {
        Genre genre = null;
        try {
            DAO<Genre> genreDAO = daoCreator.getDAO(Genre.class);
            List<Genre> genres = genreDAO.findWhere(Map.of("genre_name",name));
            if(genres != null && genres.size() > 0)
                genre = genres.get(0);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return genre;
    }

    @Override
    public void addReleaseDate(LocalDate us, LocalDate eu, LocalDate jp, String notes) throws BusinessException {
        try {
            DAO<ReleaseDate> releaseDateDAO = daoCreator.getDAO(ReleaseDate.class);
            ReleaseDate releaseDate = new ReleaseDate(us, eu, jp, notes);
            releaseDateDAO.create(releaseDate);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public void addGame(String name, String description, ReleaseDate releaseDate, Genre genre) throws BusinessException {

        try {
            DAO<Game> gameDAO = daoCreator.getDAO(Game.class);
            Game game = new Game(name, description, releaseDate, genre);
            gameDAO.create(game);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public List<Genre> getAllGenres() throws BusinessException {
        List<Genre> genres = null;
        try {
            DAO<Genre> genreDAO = daoCreator.getDAO(Genre.class);
            genres = genreDAO.findAll();
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return genres;
    }

    @Override
    public List<Game> getAllGames() throws BusinessException {
        List<Game> games = null;
        try {
            DAO<Game> gameDAO = daoCreator.getDAO(Game.class);
            games = gameDAO.findAll();
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return games;
    }

    @Override
    public Game findGame(int id) throws BusinessException {
        Game game = null;
        try {
            DAO<Game> gameDAO = daoCreator.getDAO(Game.class);
            game = gameDAO.find(id);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return game;
    }

    @Override
    public void removeGame(int id) throws BusinessException {
        try {
            DAO<Game> gameDAO = daoCreator.getDAO(Game.class);
            Game game = gameDAO.find(id);
            gameDAO.delete(game);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public Game updateGame(int id) throws BusinessException {
        Game game = null;
        try {
            DAO<Game> gameDAO = daoCreator.getDAO(Game.class);
            game = gameDAO.find(id);
            gameDAO.update(game);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return game;
    }

    @Override
    public List<ReleaseDate> getAllReleaseDate() throws BusinessException {
        List<ReleaseDate> releaseDates = null;
        try {
            DAO<ReleaseDate> ReleaseDateDAO = daoCreator.getDAO(ReleaseDate.class);
            releaseDates = ReleaseDateDAO.findAll();
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return releaseDates;
    }
}
