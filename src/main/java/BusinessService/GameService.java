package BusinessService;

import Models.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public interface GameService {

    void addGenre(String name) throws BusinessException;

    Genre findGenreByName(String name) throws BusinessException;

    void addReleaseDate(LocalDate us, LocalDate eu, LocalDate jp, String notes) throws BusinessException;

    void addGame(String name, String description, ReleaseDate releaseDate,Genre genre) throws BusinessException;

    List<Genre> getAllGenres() throws BusinessException;

    List<Game> getAllGames() throws BusinessException;

    Game findGame(int id) throws BusinessException;

    void removeGame(int id) throws  BusinessException;
    Game updateGame(int id) throws BusinessException;

    List<ReleaseDate> getAllReleaseDate ()throws BusinessException;
}
