package Models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PublisherTest {
    private static Publisher publisher = new Publisher();
    private static Game game = new Game();

    @Test
    void getName() {
        game = new Game(5, "Valorant", "Description");
        publisher = new Publisher( "Action", game);
        assertEquals("Action", publisher.getName());
    }

    @Test
    void setName() {
        publisher.setName("FPS");
        assertEquals("FPS", publisher.getName());
    }

    @Test
    void getGame() {
        game = new Game(5, "Valorant", "Description");
        publisher = new Publisher( "Action", game);
        assertEquals(game, publisher.getGame());
    }

    @Test
    void setGame() {
        game = new Game(2, "Apex", "adadadad");
        publisher.setGame(game);
        assertEquals(game, publisher.getGame());
    }
}