package Models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SystemImplementationTest {

    private static SystemImplementation systemImplementation = new SystemImplementation();
    private static Game game = new Game();

    @Test
    void getName() {
        systemImplementation = new SystemImplementation( "Action");
        assertEquals("Action", systemImplementation.getName());
    }

    @Test
    void setName() {
        game = new Game(5, "Valorant", "Description");
        systemImplementation.setName("Windows", game);
        assertEquals("Windows", systemImplementation.getName());
    }

}