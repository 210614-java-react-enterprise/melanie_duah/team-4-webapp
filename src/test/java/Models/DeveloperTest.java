package Models;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeveloperTest {

    private static Developer dev = new Developer();
    private static Game game = new Game();

    @Test
    void getId() {
        dev = new Developer(1, game, "EA");
        int id = dev.getId();
        assertEquals(1, id);
    }

    @Test
    void setId() {
        dev.setId(5);
        int id = dev.getId();
        assertEquals(5, id);
    }

    @Test
    void getGame() {
        game = new Game(5, "Valorant", "Description");
        dev = new Developer(1, game, "EA");
        assertEquals(game, dev.getGame());
    }

    @Test
    void setGame() {
        game = new Game(5, "Valorant", "Description");
        dev.setGame(game);
        assertEquals(game, dev.getGame());
    }

    @Test
    void getStudio_name() {
        dev = new Developer(1, game, "EA");
        String name = dev.getStudio_name();
        assertEquals("EA", name);
    }

    @Test
    void setStudio_name() {
        dev.setStudio_name("EA");
        String name = dev.getStudio_name();
        assertEquals("EA", name);
    }
}