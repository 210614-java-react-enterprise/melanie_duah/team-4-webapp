package BusinessService;

import Models.*;
import dataaccess.DAO;
import dataaccess.DAOCreator;
import dataaccess.DAOException;
import dataaccess.DataSourceException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DeveloperServiceImplTest {

    @Mock
    DAOCreator daoCreator;

    @BeforeEach
    void initializeTest() {
        openMocks(this);
    }

    @Test
    @DisplayName("Add developer should throw no exceptions")
    void addDeveloper() throws DataSourceException {
        //Arrange
        DAO<Developer> mockDeveloperDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Developer.class)).thenReturn(mockDeveloperDAO);
        DeveloperServiceImpl developerService = new DeveloperServiceImpl(daoCreator);

        //Act
        Executable addDeveloper = () -> developerService.addDeveloper(null,null ,null);

        //Assert
        assertDoesNotThrow(addDeveloper);
    }

    @Test
    @DisplayName("Add system should throw no exceptions")
    void addSystem() throws DataSourceException {
        //Arrange
        DAO<SystemImplementation> mockSystemImplementationDAO =  mock(DAO.class);
        when(daoCreator.getDAO(SystemImplementation.class)).thenReturn(mockSystemImplementationDAO);
        DeveloperServiceImpl developerService = new DeveloperServiceImpl(daoCreator);

        //Act
        Executable addSystemImplementation = () -> developerService.addSystem(null);

        //Assert
        assertDoesNotThrow(addSystemImplementation);
    }

    @Test
    void getAllDeveloper() throws BusinessException, DAOException, DataSourceException {
        //Arrange
        List<Developer> expectedDevelopers = Arrays.asList(new Developer(), new Developer());
        DAO<Developer> mockDeveloperDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Developer.class)).thenReturn(mockDeveloperDAO);
        when(mockDeveloperDAO.findAll()).thenReturn(expectedDevelopers);
        DeveloperServiceImpl developerService = new DeveloperServiceImpl(daoCreator);

        //Act
        List<Developer> returnedDeveloper = developerService.getAllDeveloper( null,null,null);

        //Assert
        assertIterableEquals(expectedDevelopers, returnedDeveloper);
    }

    @Test
    void findDeveloper() throws BusinessException, DAOException, DataSourceException {
        //Arrange
        DAO<Developer> mockDeveloperDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Developer.class)).thenReturn(mockDeveloperDAO);
        Developer developer = new Developer();
       developer.setId(1);
        when(mockDeveloperDAO.find(1)).thenReturn(developer);
        DeveloperServiceImpl developerService = new DeveloperServiceImpl(daoCreator);

        //Act
        Developer returnedDeveloper = developerService.findDeveloper(1);

        //Assert
        assertEquals(1, returnedDeveloper.getId());
    }

    @Test
    @DisplayName("Remove developer should throw no exceptions")
    void removeDeveloper() throws DataSourceException {
        //Arrange
        DAO<Developer> mockDeveloperDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Developer.class)).thenReturn(mockDeveloperDAO);
        DeveloperServiceImpl developerService = new DeveloperServiceImpl(daoCreator);

        //Act
        Executable removeDeveloper = () -> developerService.removeDeveloper(1);

        //Assert
        assertDoesNotThrow(removeDeveloper);
    }

    @Test
    @DisplayName("Update developer should throw no exceptions")
    void updateDeveloper() throws DataSourceException {
        //Arrange
        DAO<Developer> mockDeveloperDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Developer.class)).thenReturn(mockDeveloperDAO);
        DeveloperServiceImpl developerService = new DeveloperServiceImpl(daoCreator);

        //Act
        Executable upDateDeveloper = () -> developerService.updateDeveloper(1);

        //Assert
        assertDoesNotThrow(upDateDeveloper);
    }
}