# Custom ORM and Gaming Catalog Web Application

## Project Description
This project involves a custom ORM package that allows any client application to perform basic CRUD operations. A Gaming catalog application is built on top of this ORM which catalogs games of varios genres, their developers and release information.

## Technologies Used

* AWS RDS
* Apache Tomcat - version 10.0
* Jakarta EE Servlets
* Log4J - version 2.14.1
* JUnit Jupiter - version 5.7.1
* Mockito - version 3.9.0
* Apache Maven for dependency management
* Jetbrains IntelliJ IDEA 2021.2 Community Edition

## Features

* Partial JPA Compliant Annotations for Entity classes
* Generic Data Access Objects for performing CRUD operations on entities
* JDBC Connection Pooling
* ORM in the current implementation uses a database-first approach, i.e. the database schema must exist before entities are created
* Jakarta EE client application for gaming catalogs 

To-do list:
* Enable code-first support in ORM to enable dynamic creation of tables and their relationships based on annotations
* Expand the list of supported JPA compliant annotations
* Provide support for basic Method Queries

## Getting Started
   
* Do a git clone https://gitlab.com/210614-java-react-enterprise/melanie_duah/team-4-webapp.git
* Install JDK version 16+
* Install an IDE - IntelliJ IDEA (recommended as this is what is used for this project), Eclipse or Netbeans
* Install Apache Tomcat 10+

## Usage

 ### To use ORM
 * Initialize Connection Pool instance
 * and Create a DAO<T> instance where T is the model class using the DAOCreator factory
 * Perform CRUD operations for model class T as needed

## Contributors

 ### Here is the list of people who have contributed to this project.

* Melanie Duah
* Curtis Greene-Morgan
* Apurv Patel

## License
This project uses the following license: [APACHE LICENSE, VERSION 2.0](<https://www.apache.org/licenses/LICENSE-2.0.txt>)

